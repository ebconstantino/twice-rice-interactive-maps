module.exports = {
  name: 'hack4rice-default',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/hack4rice-default',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
