import { Component } from '@angular/core';

@Component({
  selector: 'h4r-d-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'hack4rice-default';
}
