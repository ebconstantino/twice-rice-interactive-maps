module.exports = {
  name: 'hack4rice-default-backend',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/hack4rice-default-backend'
};
